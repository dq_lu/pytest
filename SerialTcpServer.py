from socket import *
from SerialPort import MSerialPort
import _thread as thread

class SerialTcpServer:
    def __init__(self,addr,port):
        self.addr=addr
        self.port=port

    def start(self):
        server = socket(AF_INET, SOCK_STREAM)
        server.bind((self.addr, self.port))
        server.listen(1)
        self.sock, addr = server.accept()
        print("[+] connected from ", addr)
        self.sock.send(b'Hello')

    def send(self,data):
        print("串口收到==> " + data)
        self.sock.send(data)

        # while True:
        #     r = self.sock.recv(1024)
        #     print("[+] message:", r)
        #     self.sock.send(b"you said: " + r)

    def close(self):
      self.server.close()


if __name__ == '__main__':
    mSerialTcpServer=SerialTcpServer('localhost',5555)
    mSerialTcpServer.start()

    mSerial = MSerialPort('/dev/ttyAMA0', 115200)
    thread.start_new_thread(mSerial.read_data, mSerialTcpServer.send())

