import numpy as np
import matplotlib.pylab as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

if __name__ == '__main__':
    pi=plt.pi
    x = np.linspace(0, 2 * pi, 1000)
    y = np.abs(np.cos(x))

    xmajorLocator = MultipleLocator(pi / 4)  # 将x主刻度标签设置为pi/4的倍数
    xmajorFormatter = FormatStrFormatter('%1.1f')  # 设置x轴标签文本的格式
    xminorLocator = MultipleLocator(pi / 8)  # 将x轴次刻度标签设置为pi/8的倍数

    ymajorLocator = MultipleLocator(0.2)  # 将y轴主刻度标签设置为0.2的倍数
    ymajorFormatter = FormatStrFormatter('%1.1f')  # 设置y轴标签文本的格式
    yminorLocator = MultipleLocator(0.1)  # 将此y轴次刻度标签设置为0.1的倍数

    ax1 = plt.subplot(121)  # 1代表行，2代表列，所以一共有2个图，1代表此时绘制第二个图。其中ax1是为了坐标轴主次刻度大小的设置
    plt.plot(x, y)  # 绘制x-y直角坐标图
    # 设置主刻度标签的位置,标签文本的格式
    ax1.xaxis.set_major_locator(xmajorLocator)
    ax1.xaxis.set_major_formatter(xmajorFormatter)
    ax1.yaxis.set_major_locator(ymajorLocator)
    ax1.yaxis.set_major_formatter(ymajorFormatter)

    # 显示次刻度标签的位置,没有标签文本
    ax1.xaxis.set_minor_locator(xminorLocator)
    ax1.yaxis.set_minor_locator(yminorLocator)
    ax1.xaxis.grid(True, which='major')  # x坐标轴的网格使用主刻度
    ax1.yaxis.grid(True, which='minor')  # y坐标轴的网格使用次刻度

    ax2 = plt.subplot(122, polar=True)  # 1代表行，2代表列，所以一共有2个图，2代表此时绘制第二个图。polar=True表示绘制极坐标图，其中ax2是为了坐标轴主次刻度大小的设置。
    plt.plot(x, y)  # x代表角度（弧度制表示），y代表与原点的距离（需为正）。

    plt.show()