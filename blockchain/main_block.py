import hashlib as hasher
import datetime as date


class Block:
    def __init__(self,index,time,data,previous_hash):
        self.index=index
        self.time=time
        self.data=data
        self.previous_hash=previous_hash
        self.hash=self.hash_block()

    def hash_block(self):
        # md = hasher.md5()
        # md.update(bytes(str(self.index) + str(self.time) + str(self.data) + str(self.previous_hash), encoding='utf-8'))
        # return md.hexdigest()

        sha=hasher.sha256()
        sha.update((str(self.index)
                    +str(self.time)
                    +str(self.data)
                    +str(self.previous_hash))
                   .encode("utf8"))
        return sha.hexdigest()




def create_genesis_block():
    return Block(0, date.datetime.now(), 'Genesis Block', '0')


def next_block(last_block):
    index = last_block.index + 1
    return Block(index, date.datetime.now(),
                 "hi,i'm block " + str(index),
                 last_block.hash
                 )

list_blockchain=[create_genesis_block()]
previous_block=list_blockchain[0]

num_of_block_add=20

for i in range(0,num_of_block_add):
    cur_block=next_block(previous_block)
    list_blockchain.append(cur_block)
    previous_block=cur_block

    print('添加#{}号区块链!'.format(cur_block.index))
    print('hash:'+cur_block.hash)